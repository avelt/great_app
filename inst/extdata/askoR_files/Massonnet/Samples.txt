ID	file	condition	species	treatment
Passerina_bbch89_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631938.txt	Passerina_bbch89	Passerina	bbch89
Passerina_bbch89_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631937.txt	Passerina_bbch89	Passerina	bbch89
Passerina_bbch89_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631936.txt	Passerina_bbch89	Passerina	bbch89
Passerina_bbch85_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631935.txt	Passerina_bbch85	Passerina	bbch85
Passerina_bbch85_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631934.txt	Passerina_bbch85	Passerina	bbch85
Passerina_bbch85_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631933.txt	Passerina_bbch85	Passerina	bbch85
Passerina_bbch77_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631932.txt	Passerina_bbch77	Passerina	bbch77
Passerina_bbch77_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631931.txt	Passerina_bbch77	Passerina	bbch77
Passerina_bbch77_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631930.txt	Passerina_bbch77	Passerina	bbch77
Passerina_bbch75_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631929.txt	Passerina_bbch75	Passerina	bbch75
Passerina_bbch75_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631928.txt	Passerina_bbch75	Passerina	bbch75
Passerina_bbch75_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631927.txt	Passerina_bbch75	Passerina	bbch75
Moscato_Bianco_bbch89_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631926.txt	Moscato_Bianco_bbch89	Moscato_Bianco	bbch89
Moscato_Bianco_bbch89_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631925.txt	Moscato_Bianco_bbch89	Moscato_Bianco	bbch89
Moscato_Bianco_bbch89_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631924.txt	Moscato_Bianco_bbch89	Moscato_Bianco	bbch89
Moscato_Bianco_bbch85_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631923.txt	Moscato_Bianco_bbch85	Moscato_Bianco	bbch85
Moscato_Bianco_bbch85_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631922.txt	Moscato_Bianco_bbch85	Moscato_Bianco	bbch85
Moscato_Bianco_bbch85_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631921.txt	Moscato_Bianco_bbch85	Moscato_Bianco	bbch85
Moscato_Bianco_bbch77_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631920.txt	Moscato_Bianco_bbch77	Moscato_Bianco	bbch77
Moscato_Bianco_bbch77_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631919.txt	Moscato_Bianco_bbch77	Moscato_Bianco	bbch77
Moscato_Bianco_bbch77_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631918.txt	Moscato_Bianco_bbch77	Moscato_Bianco	bbch77
Moscato_Bianco_bbch75_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631917.txt	Moscato_Bianco_bbch75	Moscato_Bianco	bbch75
Moscato_Bianco_bbch75_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631916.txt	Moscato_Bianco_bbch75	Moscato_Bianco	bbch75
Moscato_Bianco_bbch75_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631915.txt	Moscato_Bianco_bbch75	Moscato_Bianco	bbch75
Glera_bbch89_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631914.txt	Glera_bbch89	Glera	bbch89
Glera_bbch89_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631913.txt	Glera_bbch89	Glera	bbch89
Glera_bbch89_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631912.txt	Glera_bbch89	Glera	bbch89
Glera_bbch85_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631911.txt	Glera_bbch85	Glera	bbch85
Glera_bbch85_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631910.txt	Glera_bbch85	Glera	bbch85
Glera_bbch85_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631909.txt	Glera_bbch85	Glera	bbch85
Glera_bbch77_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631908.txt	Glera_bbch77	Glera	bbch77
Glera_bbch77_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631907.txt	Glera_bbch77	Glera	bbch77
Glera_bbch77_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631906.txt	Glera_bbch77	Glera	bbch77
Glera_bbch75_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631905.txt	Glera_bbch75	Glera	bbch75
Glera_bbch75_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631904.txt	Glera_bbch75	Glera	bbch75
Glera_bbch75_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631903.txt	Glera_bbch75	Glera	bbch75
Garganega_bbch89_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631902.txt	Garganega_bbch89	Garganega	bbch89
Garganega_bbch89_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631901.txt	Garganega_bbch89	Garganega	bbch89
Garganega_bbch89_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631900.txt	Garganega_bbch89	Garganega	bbch89
Garganega_bbch85_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631899.txt	Garganega_bbch85	Garganega	bbch85
Garganega_bbch85_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631898.txt	Garganega_bbch85	Garganega	bbch85
Garganega_bbch85_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631897.txt	Garganega_bbch85	Garganega	bbch85
Garganega_bbch77_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631896.txt	Garganega_bbch77	Garganega	bbch77
Garganega_bbch77_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631895.txt	Garganega_bbch77	Garganega	bbch77
Garganega_bbch77_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631894.txt	Garganega_bbch77	Garganega	bbch77
Garganega_bbch75_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631893.txt	Garganega_bbch75	Garganega	bbch75
Garganega_bbch75_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631892.txt	Garganega_bbch75	Garganega	bbch75
Garganega_bbch75_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631891.txt	Garganega_bbch75	Garganega	bbch75
Vermentino_bbch89_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631890.txt	Vermentino_bbch89	Vermentino	bbch89
Vermentino_bbch89_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631889.txt	Vermentino_bbch89	Vermentino	bbch89
Vermentino_bbch89_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631888.txt	Vermentino_bbch89	Vermentino	bbch89
Vermentino_bbch85_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631887.txt	Vermentino_bbch85	Vermentino	bbch85
Vermentino_bbch85_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631886.txt	Vermentino_bbch85	Vermentino	bbch85
Vermentino_bbch85_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631885.txt	Vermentino_bbch85	Vermentino	bbch85
Vermentino_bbch77_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631884.txt	Vermentino_bbch77	Vermentino	bbch77
Vermentino_bbch77_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631883.txt	Vermentino_bbch77	Vermentino	bbch77
Vermentino_bbch77_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631882.txt	Vermentino_bbch77	Vermentino	bbch77
Vermentino_bbch75_1	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631881.txt	Vermentino_bbch75	Vermentino	bbch75
Vermentino_bbch75_2	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631880.txt	Vermentino_bbch75	Vermentino	bbch75
Vermentino_bbch75_3	/data/share/GREAT/GREAT/scripts/great_app_test/extdata/askoR_files/Massonnet/SRR1631879.txt	Vermentino_bbch75	Vermentino	bbch75
