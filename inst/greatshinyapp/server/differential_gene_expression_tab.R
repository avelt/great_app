#################################################################################
# ANALYSE ASKOR
#################################################################################
observeEvent(input$launch_askor, {

    output$download_askor_ui <- renderUI({
      actionButton('download_askor', 'Download report analysis')
    })

  shinyInput <- function(FUN, len, id, ...) {
    inputs <- character(len)
    for (i in seq_len(len)) {
      inputs[i] <- as.character(FUN(paste0(id, i), ...))
    }
    inputs
  }

  radio_askoR=input$radio_askoR
  radio_askoR_table=radio_askoR

  if(radio_askoR != "No_analysis"){

    #################################################################################
    # Table résumé avec lien de téléchargement du fichier d'expression différentielle
    #################################################################################
    output$table_resume <- renderText({
      "Number of genes differentially expressed in each comparison."
    })
    filename_resume=system.file("extdata", paste(paste("/askoR_files/",radio_askoR,sep=""),"resume_table.txt",sep="/"), package = "great")
    resume_table=read.table(filename_resume, sep="\t", header=T)
    number_comp=length(resume_table$comparison)

    my_data_table <- reactiveValues(data = data.frame(
      Comparison = resume_table[,1],
      Genes_up_regulated = resume_table[,2],
      Genes_down_regulated = resume_table[,3],
      Actions = shinyInput(actionButton, number_comp, 'button_', label = "Download complete DE table", onclick = 'Shiny.onInputChange(\"select_button\",  this.id)' ),
      stringsAsFactors = FALSE,
      row.names = 1:number_comp
    ))

    observeEvent(input$select_button, {
      selectedButton <- as.numeric(strsplit(input$select_button, "_")[[1]][2])
      name = my_data_table$data[selectedButton,1]
      filename=system.file("extdata", paste(paste(paste("/askoR_files/",radio_askoR,sep=""),name,sep="/"),".txt.rds",sep=""), package = "great")
      file_download=readRDS(filename)

      myModal <- function() {
        div(id = "select_button",
          modalDialog(downloadButton("download_DE_table","Download complete DE table"),
          br(),
          br(),
          easyClose = TRUE, title = "Download complete DE table")
        )
      }

      showModal(myModal())

      output$download_DE_table <- downloadHandler(
        filename = function() {
          paste("DE_table_", name, ".csv", sep="")
        },
        content = function(file) {
          write.csv(file_download, file)
        }
      )
    })

    output$my_data_table <- DT::renderDataTable(
      my_data_table$data, server = FALSE, escape = FALSE, selection = 'none'
    )

    #################################################################################
    # Download du PDF de résultats askoR
    #################################################################################

      observeEvent(input$download_askor, {
      filename_pdf=system.file("extdata", paste(paste("/askoR_files/",radio_askoR,sep=""),"out.pdf",sep="/"), package = "great")

      myModal_pdf <- function() {
        div(id = "download_askor",
          modalDialog(downloadButton("download_askor_pdf","Download analysis report"),
          br(),
          br(),
          easyClose = TRUE, title = "Download analysis report")
        )
      }

      showModal(myModal_pdf())

      output$download_askor_pdf <- downloadHandler(
        filename = function() {
          paste("Report_analysis_", radio_askoR, ".pdf", sep="")
        },
        content = function(file) {
          file.copy(filename_pdf, file)
        }
      )
    })

    #################################################################################
    # Subset de GREAT database pour les échantillons sélectionnés
    #################################################################################
    output$table_info <- renderText({
      "Information about selected samples."
    })
    cond_file=system.file("extdata", "/askoR_files/configuration_file.txt", package = "great")
    cond=read.table(cond_file, sep="\t", header=T)
    SRR=cond[cond$First_author == radio_askoR,2]
    info_subset=info_data_table[info_data_table$File_name %in% SRR,]
    output$my_info_table_askor = renderDataTable(info_subset,caption="Informations table of samples",extensions = c('Buttons'),
      options = list(
      dom = 'Bfrtip',
      buttons = c('csv', 'excel'),
      filename= paste("GREAT_samples_information_", format(Sys.time(), "%Y%m%d_%H%M"), sep=""),
      pageLength = 50
    ))
  }
})
